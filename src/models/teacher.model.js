const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let teacherSchema = new Schema({
    name: { type: String, required: false },
    subject: { type: String, required: false },
    email: { type: String, required: false },
    password: { type: String, required: false },
},
    {
        collection: 'teacher'
    })
module.exports = mongoose.model('Teacher', teacherSchema)