const { number } = require('@hapi/joi');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let studentSchema = new Schema({
    name: { type: String, required: false },
    reg_no: { type: Number, required: false },
    subject_1: { type: Number, required: false },
    subject_2: { type: Number, required: false },
    subject_3: { type: Number, required: false }, 
    total: { type: Number, required: false },
    teacher_id: { type: String, required: false },
},
    {
        collection: 'student'
    })
module.exports = mongoose.model('Student', studentSchema)